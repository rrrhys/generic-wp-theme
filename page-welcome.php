<?php
/* Template Name: Welcome Page */
?>

<?php get_header(); ?>
<section id="content" role="main">
<div class="container">
    <div class="row hero-callout">
        <div class="col-sm ">
            <h2><?php the_field("hero-header"); ?></h2>
            <?php the_field("hero-body"); ?>
            <div class="row" style="margin-top: 20px;">
                <div class="col-sm">
                    <a href="<?php echo get_custom("get_started_button_link"); ?>" class="btn btn-primary btn-block btn-rounded btn-extra-padding"><?php echo get_custom("get_started_button_text"); ?></a>
                </div>
                <div class="col-sm">
                    <a href="<?php the_field("homepage_secondary_button_link"); ?>" class=" btn btn-outline-primary btn-block btn-rounded btn-extra-padding"><?php the_field("homepage_secondary_button_label"); ?></a>
                </div>
            </div>
        </div>
        <div class="col-sm" style="text-align:right;">
            <img src="<?php the_field("hero_image"); ?>" class="img-fluid" alt="Responsive image">
        </div>
    </div>
</div>

</section>

<?php if (get_field('show_social_proof')) : ?>
<div class="container">
    <div class="row">
        <div class="col-sm">
            <section id="proof" style="min-height: 80px; border: 1px solid #ccc;">
            <?php the_field("social-proof"); ?>
            </section>
        </div>
    </div>

</div>
<?php endif; ?>

<section id="feature_list" style="padding-left: 40px; padding-right: 40px;">
    <section id="feature_1" class="homepage-feature-row">
    <div class="row">
        <div class="col-sm"><img src="<?php the_field("home_section_1_image"); ?>" class="img-fluid" alt="Responsive image"></div>

        <div class="col-sm">
            <h2 class="homepage-feature-heading"><?php the_field("home_section_1_header"); ?></h2>
            <p class="feature-copy"><?php the_field("home_section_1_body"); ?></p>
        </div>
    </div>
    </section>
    <section id="feature_2" class="homepage-feature-row">
    <div class="row">

        <div class="col-sm">
            <h2 class="homepage-feature-heading"><?php the_field("home_section_2_header"); ?></h2>
            <p class="feature-copy"><?php the_field("home_section_2_body"); ?></p>
        </div>
        <div class="col-sm" style="text-align:right;"><img src="<?php the_field("home_section_2_image"); ?>" class="img-fluid" alt="Responsive image"></div>

    </div></section>
    <section id="feature_3" class="homepage-feature-row">
    <div class="row">
        <div class="col-sm"><img src="<?php the_field("home_section_3_image"); ?>" class="img-fluid" alt="Responsive image"></div>

        <div class="col-sm">
            <h2 class="homepage-feature-heading"><?php the_field("home_section_3_header"); ?></h2>
            <p class="feature-copy"><?php the_field("home_section_3_body"); ?></p>
        </div>
    </div></section>
</section>
<?php get_footer(); ?>

