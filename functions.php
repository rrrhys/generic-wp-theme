<?php
define('ACF_EARLY_ACCESS', '5');
add_action('after_setup_theme', 'SkeletonSaas_setup');
function SkeletonSaas_setup()
{
    load_theme_textdomain('SkeletonSaas', get_template_directory() . '/languages');
    add_theme_support('title-tag');
    add_theme_support('automatic-feed-links');
    add_theme_support('post-thumbnails');
    global $content_width;
    if (!isset($content_width)) {
        $content_width = 640;
    }

    register_nav_menus(
        array(
            'main-menu' => __('Main Menu', 'SkeletonSaas'),
            'get-started-main-menu' => __('Get Started Main Menu', 'SkeletonSaas'),
            'footer-1' => __('Footer 1 Menu', 'SkeletonSaas'),
            'footer-2' => __('Footer 2 Menu', 'SkeletonSaas'),

        )
    );
}


add_action('wp_enqueue_scripts', 'SkeletonSaas_load_scripts');
function SkeletonSaas_load_scripts()
{
    wp_enqueue_script('jquery');
// wp_enqueue_style('theme-style', get_stylesheet_directory_uri() . '/css/style.css', array(), 20141119);

    wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.css', array(), 20141119);
    wp_enqueue_style('bootstrap-overloads', get_stylesheet_directory_uri() . '/css/bootstrap-overloads.css', array(), 20141119);
// all scripts
wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '20120206', true);
wp_enqueue_script('theme-script', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '20120206', true);

}

function get_recent_posts(){
$args = array(
    'numberposts' => 20,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'include' => '',
    'exclude' => '',
    'meta_key' => '',
    'meta_value' => '',
    'post_type' => 'post',
    'post_status' => 'publish',
    'suppress_filters' => true,
);

$recent_posts = wp_get_recent_posts($args, ARRAY_A);


return $recent_posts;
}
function output_recent_posts()
{
    $recent_posts = get_recent_posts();

    $html = "<ul class='menu'>";
    $html .= "<li class='nav-link'><strong>Recent Posts</strong></li>";
    foreach ($recent_posts as $post) {
        if ($post) {
            $html .= "<li class='nav-item'><a class='nav-link' href='" . get_permalink($post['ID']) . "'>" . $post['post_title'] . "</a></li>";

        }
    }

    $html .= "</ul>";
    return $html;

}

add_action('init', 'add_excerpts_to_pages');
function add_excerpts_to_pages()
{
    add_post_type_support('page', 'excerpt');
}


function add_menuclass($ulclass) {
   return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
}
add_filter('wp_nav_menu','add_menuclass');

function add_classes_on_li($classes, $item, $args)
{
    if($args->menu->name !== "Footer Menu 2"){
$classes = ['nav-item'];

    }
    return $classes;
}
add_filter('nav_menu_css_class', 'add_classes_on_li', 1, 3);


function wp_42573_fix_template_caching( WP_Screen $current_screen ) {
	// Only flush the file cache with each request to post list table, edit post screen, or theme editor.
	if ( ! in_array( $current_screen->base, array( 'post', 'edit', 'theme-editor' ), true ) ) {
		return;
	}
	$theme = wp_get_theme();
	if ( ! $theme ) {
		return;
	}
	$cache_hash = md5( $theme->get_theme_root() . '/' . $theme->get_stylesheet() );
	$label = sanitize_key( 'files_' . $cache_hash . '-' . $theme->get( 'Version' ) );
	$transient_key = substr( $label, 0, 29 ) . md5( $label );
	delete_transient( $transient_key );
}
add_action( 'current_screen', 'wp_42573_fix_template_caching' );



add_action('comment_form_before', 'SkeletonSaas_enqueue_comment_reply_script');
function SkeletonSaas_enqueue_comment_reply_script()
{
    if (get_option('thread_comments')) {wp_enqueue_script('comment-reply');}
}
add_filter('the_title', 'SkeletonSaas_title');
function SkeletonSaas_title($title)
{
    if ($title == '') {
        return '&rarr;';
    } else {
        return $title;
    }
}
add_filter('wp_title', 'SkeletonSaas_filter_wp_title');
function SkeletonSaas_filter_wp_title($title)
{
    return $title . esc_attr(get_bloginfo('name'));
}
add_action('widgets_init', 'SkeletonSaas_widgets_init');
function SkeletonSaas_widgets_init()
{
    register_sidebar(array(
        'name' => __('Sidebar Widget Area', 'SkeletonSaas'),
        'id' => 'primary-widget-area',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => "</li>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}
function SkeletonSaas_custom_pings($comment)
{
    $GLOBALS['comment'] = $comment;
    ?>
<li <?php comment_class();?> id="li-comment-<?php comment_ID();?>"><?php echo comment_author_link(); ?></li>
<?php
}


