<div class="clear"></div>


<?php 
global $post;
$post_slug = $post && $post->post_name;

if(!$post_slug || $post_slug !== "get-started"):?>
<div class="last-sell" style="min-height: 200px; margin-top: 80px; margin-bottom: 80px;text-align:center;">
    <h2><?php echo get_custom("get_started_header_text");?>
</h2>
    <p>
    <?php echo get_custom("get_started_header_body");?>

    </p>
    <a href="<?php echo get_custom("get_started_button_link"); ?>" class="btn btn-primary btn-rounded btn-extra-padding"><?php echo get_custom("get_started_button_text"); ?></a>
</div>
<?php endif; ?>
</div>
<footer id="footer" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-sm footer-menu"> <?php wp_nav_menu( array( 'theme_location' => 'footer-1' ) ); ?></div>
            <div class="col-sm footer-menu"> <?php wp_nav_menu( array( 'theme_location' => 'footer-2' ) ); ?></div>
            <div class="col-sm footer-menu"> <?php echo output_recent_posts(); ?></div>
            <div class="col-sm footer-notices">
                <ul>
                    <li>FB</li>
                    <li>TW</li>
                    <li>LI</li>
                </ul>
                <div id="copyright">
<?php echo sprintf( __( '%1$s %2$s %3$s. All Rights Reserved.', 'SkeletonSaas' ), '&copy;', date( 'Y' ), esc_html( get_bloginfo( 'name' ) ) ); ?>
</div>
            </div>
        </div>
    </div>

</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>