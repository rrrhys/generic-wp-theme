<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
<header id="header" role="banner">

<nav class="navbar navbar-expand-lg navbar-light bg-white with-shadow">
<div class="container">
  
      <a class="navbar-brand" href="/" id="site-title" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home"><?php echo esc_html( get_bloginfo( 'name' ) ); ?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExample05">
        <?php wp_nav_menu(array('theme_location' => 'main-menu', 'menu_class'=>'navbar-nav', 'container_class'=>'ml-auto'));?>
            <?php wp_nav_menu(array('theme_location' => 'get-started-main-menu', 'menu_class'=>'navbar-nav ', 'container_class'=>'force-right cta-menu-wrapper'));?>
      </div>
</div>

    </nav>

 
</header>
<div id="container" class="container container-under-fixed-header">