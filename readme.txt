- - THEME - -

SkeletonSaas WordPress Theme. Built from BlankSlate
Demo: http://insert/demo/link
Download: https://insert/download/link

- - DESCRIPTION - -

This theme is aimed at XYZ.

The bare essentials of a WordPress theme

https://www.codeworkshop.com.au

- - COPYRIGHT & LICENSE - -

GNU General Public License | https://www.gnu.org/licenses/gpl.html

- - SUPPORT - -

sales@codeworkshop.com.au