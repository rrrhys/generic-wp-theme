<?php
/* Template Name: Blog Page */
?>

<?php get_header(); ?>

<h1 class="entry-title"><?php the_title(); ?></h1>

<?php

function get_start_of_post($post, $words){
    $post_parts = get_extended($post['post_content']);
    $post_start = $post_parts['main'];

    $post_start = wp_trim_words( $post_start, $num_words = $words, $more = null );

    return $post_start;
}


$args = array(
    'numberposts' => '10',
    'post_type' => 'post',
    'post_status' => 'publish'
);


$posts = get_recent_posts($args);
$first_post = $posts[0];

$post_start = get_start_of_post($first_post, 55);
?>
<section id="content" role="main">
    <div class="container">
    <div class="row">
        <div class="col-md-8" style="text-align:center;">
<?php echo get_the_post_thumbnail($first_post['ID'],null,"class=img-fluid") ?>

        </div>
        <div class="col-md-4">
        <h2 style="font-size:48px">
<a href="<?php echo get_permalink($first_post['ID']); ?>"><?php echo $first_post['post_title'];?></a>

        </h2>
        <p style="font-size:28px;;font-weight: 400;color:#666666;"><?php echo $post_start?></p>
        
        </div>
    </div>
    <?php for($i = 1; $i < count($posts); $i++):

$post_start = get_start_of_post($posts[$i], 25);

?>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-5 col-md-offset-3">
            <h3 style="font-size: 38px;">
            <a href="<?php echo get_permalink($posts[$i]['ID']);?>"><?php echo $posts[$i]['post_title'];?></a></h3>
            <p style="font-size:24px;font-weight: 400; color: #666666;">
            <?php echo $post_start; ?></p>
</div>
            <div class="col-md-3"><?php echo get_the_post_thumbnail($posts[$i]['ID'], null, "class=img-fluid") ?>
</div>
        </div>
<?php

    endfor; ?>

    </div>
</section>
<?php get_footer(); ?>

